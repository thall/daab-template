#include <iostream>
#include <string>
#include <cstdlib>
#include "AVLMap.h"

using std::cout;
using std::string;

void doTest(bool succ, string name) {
  string ret;
  if(succ){
    ret = "\033[1;32m"+name+" test succeeded\033[0m";
  } else {
    ret = "\033[1;31m"+name+" test failed\033[0m";
  }
  cout << ret << "\n";
}

void subTest(string message) {
  cout << "\033[1;32m    "+message+" subtest\033[0m\n";
}

template<typename T>
bool basicTest(T &thing) {
  // write test
  subTest("placeholder");
  return true;
}

template<typename T>
bool stringTest(T &thing) {
  // write test
  subTest("placeholder");
  return true;
}

// template <typename T>
// bool scaleInsert(T &lst) {
//   lst.clear();
//   for(int i = 0; i<2000; ++i) {
//     lst.insert(make_pair(i,i));
//   }
//   for(int i = 0; i<2000; ++i) {
//     if(lst[i]!=i) return false;
//   }
//   subTest("insert");
//   // TODO: Check contains
//   lst.clear();
//   return true;
// }

// template <typename T>
// bool scaleErase(T &lst) {
//   lst.clear();
//   for(int i = 0; i<2000; ++i) {
//     lst.insert(make_pair(i,i));
//   }
//   for(int i = 0; i<2000; ++i) {
//     if(lst[i]!=i) return false;
//   }
//   subTest("insert");
//   if(lst.erase(20)!=1) return false;
//   if(lst.size()!=1999) return false;
//   for(int i = 0; i<2000; ++i) {
//     if((lst[i]!=i) && i!=20) return false;
//   }
//   subTest("erase");
//   lst.clear();
//   return true;
// }

int main() {
  // initialize thingy
  // doTest(basicTest(map), "int map basic");
  return 0;
}
